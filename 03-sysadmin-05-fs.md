# Домашнее задание к занятию "Файловые системы"

1. Узнайте о [sparse](https://ru.wikipedia.org/wiki/%D0%A0%D0%B0%D0%B7%D1%80%D0%B5%D0%B6%D1%91%D0%BD%D0%BD%D1%8B%D0%B9_%D1%84%D0%B0%D0%B9%D0%BB) (разряженных) файлах.

    _Разреженные – это специальные файлы, которые с большей эффективностью используют файловую систему, они не позволяют ФС занимать свободное дисковое пространство носителя, когда разделы не заполнены. То есть, «пустое место» будет задействовано только при необходимости. Пустая информация в виде нулей, будет хранится в блоке метаданных ФС. Поэтому, разреженные файлы изначально занимают меньший объем носителя, чем их реальный объем._

    _https://habr.com/ru/company/hetmansoftware/blog/553474/_

1. Могут ли файлы, являющиеся жесткой ссылкой на один объект, иметь разные права доступа и владельца? Почему?

    _Жесткая ссылка это по сути зеркальная копия объекта, наследующая его права, владельца и группу. Имеет тот же inode что и оригинальный файл. Поэтому разных владельцев и разные права оригинал и хардлинк иметь не могут._

1. Сделайте `vagrant destroy` на имеющийся инстанс Ubuntu. Замените содержимое Vagrantfile следующим:

    ```ruby
    path_to_disk_folder = './disks'

    host_params = {
        'disk_size' => 2560,
        'disks'=>[1, 2],
        'cpus'=>2,
        'memory'=>2048,
        'hostname'=>'sysadm-fs',
        'vm_name'=>'sysadm-fs'
    }
    Vagrant.configure("2") do |config|
        config.vm.box = "bento/ubuntu-20.04"
        config.vm.hostname=host_params['hostname']
        config.vm.provider :virtualbox do |v|

            v.name=host_params['vm_name']
            v.cpus=host_params['cpus']
            v.memory=host_params['memory']

            host_params['disks'].each do |disk|
                file_to_disk=path_to_disk_folder+'/disk'+disk.to_s+'.vdi'
                unless File.exist?(file_to_disk)
                    v.customize ['createmedium', '--filename', file_to_disk, '--size', host_params['disk_size']]
                end
                v.customize ['storageattach', :id, '--storagectl', 'SATA Controller', '--port', disk.to_s, '--device', 0, '--type', 'hdd', '--medium', file_to_disk]
            end
        end
        config.vm.network "private_network", type: "dhcp"
    end
    ```

    Данная конфигурация создаст новую виртуальную машину с двумя дополнительными неразмеченными дисками по 2.5 Гб.

1. Используя `fdisk`, разбейте первый диск на 2 раздела: 2 Гб, оставшееся пространство.

    _`sudo fdisk /dev/sdb`_
    
    _`n` , `[enter]` , `[enter]` , `[enter]` , `+2G`_

    _`n` , `[enter]` , `[enter]` , `[enter]` , `[enter]`_

1. Используя `sfdisk`, перенесите данную таблицу разделов на второй диск.

    _`sudo sfdisk --dump /dev/sdb > sdb.dump`_

    _`sudo sfdisk /dev/sdc < sdb.dump`_

1. Соберите `mdadm` RAID1 на паре разделов 2 Гб.

    _`sudo mdadm --create /dev/md1 -l 1 -n 2 /dev/sd{b1,c1}`_

1. Соберите `mdadm` RAID0 на второй паре маленьких разделов.

    _`sudo mdadm --create /dev/md2 -l 0 -n 2 /dev/sd[bc]2`_

1. Создайте 2 независимых PV на получившихся md-устройствах.

    _`sudo pvcreate /dev/md1 /dev/md2`_

    _`sudo pvscan`_

    ```
    PV /dev/sda3   VG ubuntu-vg       lvm2 [<62.00 GiB / 31.00 GiB free]
    PV /dev/md1                       lvm2 [<2.00 GiB]
    PV /dev/md2                       lvm2 [1018.00 MiB]
    ```

1. Создайте общую volume-group на этих двух PV.

    _`sudo vgcreate vg0 /dev/md1 /dev/md2`_

    _Проверка: `sudo vgdisplay` , `sudo pvdisplay` , `sudo vgs`_

1. Создайте LV размером 100 Мб, указав его расположение на PV с RAID0.

    _`sudo lvcreate -L 100M -n lv2 vg0 /dev/md2`_

    _Проверка: `sudo lvdisplay`_

1. Создайте `mkfs.ext4` ФС на получившемся LV.

    _`sudo mkfs.ext4 /dev/vg0/lv2`_

1. Смонтируйте этот раздел в любую директорию, например, `/tmp/new`.

    _`sudo mkdir /tmp/new`_

    _`sudo mount /dev/vg0/lv2 /tmp/new`_

    _Проверка: `df -hT | grep /dev`_

1. Поместите туда тестовый файл, например `wget https://mirror.yandex.ru/ubuntu/ls-lR.gz -O /tmp/new/test.gz`.

1. Прикрепите вывод `lsblk`.

    ```
    NAME                      MAJ:MIN RM  SIZE RO TYPE  MOUNTPOINT
    loop0                       7:0    0 63.3M  1 loop  /snap/core20/1822
    loop1                       7:1    0 67.8M  1 loop  /snap/lxd/22753
    loop2                       7:2    0 91.9M  1 loop  /snap/lxd/24061
    loop3                       7:3    0 49.9M  1 loop  /snap/snapd/18357
    loop4                       7:4    0   62M  1 loop  /snap/core20/1611
    sda                         8:0    0   64G  0 disk  
    ├─sda1                      8:1    0    1M  0 part  
    ├─sda2                      8:2    0    2G  0 part  /boot
    └─sda3                      8:3    0   62G  0 part  
      └─ubuntu--vg-ubuntu--lv 253:0    0   31G  0 lvm   /
    sdb                         8:16   0  2.5G  0 disk  
    ├─sdb1                      8:17   0    2G  0 part  
    │ └─md1                     9:1    0    2G  0 raid1 
    └─sdb2                      8:18   0  511M  0 part  
      └─md2                     9:2    0 1018M  0 raid0 
        └─vg0-lv2             253:1    0  100M  0 lvm   /tmp/new
    sdc                         8:32   0  2.5G  0 disk  
    ├─sdc1                      8:33   0    2G  0 part  
    │ └─md1                     9:1    0    2G  0 raid1 
    └─sdc2                      8:34   0  511M  0 part  
      └─md2                     9:2    0 1018M  0 raid0 
        └─vg0-lv2             253:1    0  100M  0 lvm   /tmp/new
    ```

1. Протестируйте целостность файла:

    ```bash
    root@vagrant:~# gzip -t /tmp/new/test.gz
    root@vagrant:~# echo $?
    0
    ```

1. Используя pvmove, переместите содержимое PV с RAID0 на RAID1.

    _`sudo pvmove -n /dev/vg0/lv2 /dev/md2 /dev/md1`_

1. Сделайте `--fail` на устройство в вашем RAID1 md.

    _`sudo mdadm /dev/md1 --fail /dev/sdb1`_

1. Подтвердите выводом `dmesg`, что RAID1 работает в деградированном состоянии.

    ```
    [  738.079464] md/raid1:md1: Disk failure on sdb1, disabling device.
                   md/raid1:md1: Operation continuing on 1 devices.
    ```

1. Протестируйте целостность файла, несмотря на "сбойный" диск он должен продолжать быть доступен:

    ```bash
    gzip -t /tmp/new/test.gz ;   echo $?
    0
    ```

1. Погасите тестовый хост, `vagrant destroy`.