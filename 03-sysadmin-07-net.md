# Домашнее задание к занятию "Компьютерные сети.Лекция 2"

1. Проверьте список доступных сетевых интерфейсов на вашем компьютере. Какие команды есть для этого в Linux и в Windows?

    ---

    _Linux:_

    _`ls /sys/class/net` – просто список названий интерфейсов._

    _`ifconfig` – список интерфейсов и информация о них (считается устаревшей)._

    _`ip -br link show` – краткая (-br) информация о интерфейсах._

    _`netstat -i` – интерфейсы и статистика по ним._
    
    _`cat /proc/net/dev` – интерфейсы и статистика по ним._

    _Windows:_

    _`ipconfig -all` – список интерфейсов и подробная информация о них._

    _`netsh interface ipv4 show interfaces` – список интерфейсов._

    _PowerShell:_

    _`Get-NetAdapter`_

    ---

1. Какой протокол используется для распознавания соседа по сетевому интерфейсу? Какой пакет и команды есть в Linux для этого?

    ---

    _Протокол LLDP_

    _`sudo apt -y install lldpd`_

    _`lldpctl`_

    ---

1. Какая технология используется для разделения L2 коммутатора на несколько виртуальных сетей? Какой пакет и команды есть в Linux для этого? Приведите пример конфига.

    ---

    _Технология VLAN (802.1q)_

    _`sudo apt -y install vlan`_

    _`sudo nano /etc/netplan/01-netcfg.yaml` (или `50-cloud-init.yaml`)_

    ```
    network:
        version: 2
        renderer: networkd
        ethernets:
            eth0: {}
        vlans: 
            vlan100:
                id: 100
                link: eth0
                dhcp4: yes
    ```

    _`netplan --debug generate` – проверка нашего конфигурационного файла._

    _`netplan apply`_

    ---

1. Какие типы агрегации интерфейсов есть в Linux? Какие опции есть для балансировки нагрузки? Приведите пример конфига.

    ---

    _[Типы объединения интерфейсов](https://mkdev.me/ru/posts/kak-rabotayut-seti-chast-2-otkazoustoychivost-s-teaming-rezhem-soedineniya-s-traffic-control-a-tak-zhe-tap-interfeysy-i-linux-bridge): teaming, bonding, link aggreation и port trunking._

    _Пример конфига bonding в netplan:_

    ```
    network:

        <...>

        bonds:
            bond0:
                interfaces:
                    - eth0
                    - eth1
                parameters:
                    mode: active-backup

                <...>
    ```

    _mode (опции балансировки):_

    * _**balance-rr** (задействуются оба интерфейса по очереди, распределение пакетов по принципу Round Robin)._
    * _**active-backup** (используется только один интерфейс, второй активируется в случае неработоспособности первого)._
    * _**balance-xor** (задействуются оба интерфейса по очереди, распределение пакетов на основе политики хеширования xmit_hash_policy)._
    * _**broadcast** (задействуются оба интерфейса одновременно, пакеты передаются все интерфейсы)._
    * _**802.3ad** (задействуются оба интерфейса по очереди, распределение пакетов на основе политики хеширования xmit_hash_policy)._
    * _**balance-tlb** (задействуются оба интерфейса по очереди, пакеты распределяются в соответствии с текущей нагрузкой)._
    * _**balance-alb** (адаптивное распределение нагрузки, аналогично предыдущему режиму, но с возможностью балансировать также входящую нагрузку)._

    ---

1. Сколько IP адресов в сети с маской /29 ? Сколько /29 подсетей можно получить из сети с маской /24. Приведите несколько примеров /29 подсетей внутри сети 10.10.10.0/24.

    ---

    _2^(32-29) = 8 адресов (6 адресов хостов)_

    _2^(32-24) : 2^(32-29) = 32 подсети_

    _`sudo apt -y install ipcalc`_

    _`ipcalc 10.10.10.0/29`_

    _`ipcalc 10.10.10.0/24 /29`_

    _`ipcalc 10.10.10.0/24 -s 6 6 6`_

    ```
    ...

    1. Requested size: 6 hosts
    Netmask:   255.255.255.248 = 29 11111111.11111111.11111111.11111 000
    Network:   10.10.10.0/29        00001010.00001010.00001010.00000 000
    HostMin:   10.10.10.1           00001010.00001010.00001010.00000 001
    HostMax:   10.10.10.6           00001010.00001010.00001010.00000 110
    Broadcast: 10.10.10.7           00001010.00001010.00001010.00000 111
    Hosts/Net: 6                     Class A, Private Internet

    2. Requested size: 6 hosts
    Netmask:   255.255.255.248 = 29 11111111.11111111.11111111.11111 000
    Network:   10.10.10.8/29        00001010.00001010.00001010.00001 000
    HostMin:   10.10.10.9           00001010.00001010.00001010.00001 001
    HostMax:   10.10.10.14          00001010.00001010.00001010.00001 110
    Broadcast: 10.10.10.15          00001010.00001010.00001010.00001 111
    Hosts/Net: 6                     Class A, Private Internet

    3. Requested size: 6 hosts
    Netmask:   255.255.255.248 = 29 11111111.11111111.11111111.11111 000
    Network:   10.10.10.16/29       00001010.00001010.00001010.00010 000
    HostMin:   10.10.10.17          00001010.00001010.00001010.00010 001
    HostMax:   10.10.10.22          00001010.00001010.00001010.00010 110
    Broadcast: 10.10.10.23          00001010.00001010.00001010.00010 111
    Hosts/Net: 6                     Class A, Private Internet

    ...
    ```

    ---

1. Задача: вас попросили организовать стык между 2-мя организациями. Диапазоны 10.0.0.0/8, 172.16.0.0/12, 192.168.0.0/16 уже заняты. Из какой подсети допустимо взять частные IP адреса? Маску выберите из расчета максимум 40-50 хостов внутри подсети.

    ---

    _Carrier-Grade NAT: 100.64.0.0/10_

    _`ipcalc 100.64.0.0/10 -s 50 | grep Network:`_

    ```
    Network:   100.64.0.0/10        01100100.01 000000.00000000.00000000
    Network:   100.64.0.0/26        01100100.01000000.00000000.00 000000
    ```

    _Например: **100.99.88.64/26** – лучше не выбирать подсеть из начала диапазона, чтобы уменьшить вероятность пересечения с другими подсетями._

    ---

1. Как проверить ARP таблицу в Linux, Windows? Как очистить ARP кеш полностью? Как из ARP таблицы удалить только один нужный IP?

    ---

    _Linux:_

    _`arp -n` – таблица ARP._

    _`ip neigh` – таблица ARP._

    _`sudo arp -d 10.10.10.123` – удалить адрес из таблицы ARP._

    _`sudo ip neigh del dev eth0 10.10.10.123` – удалить адрес из таблицы ARP на интерфейсе eth0._

    _`sudo ip neigh flush all` – очистить таблицу ARP._

    _Windows:_

    _`arp -a` – таблица ARP._

    _`netsh int ipv4 show neigh` – таблица ARP._

    _`arp -d 10.10.10.123 Ethernet1` – удалить адрес из таблицы ARP на интерфейсе Ethernet1._

    _`netsh int ipv4 delete neigh 10.10.10.123` – удалить адрес из таблицы ARP._

    ---
