# Домашнее задание к занятию 4. «Оркестрация группой Docker-контейнеров на примере Docker Compose»

## Задача 1

Создайте собственный образ любой операционной системы (например ubuntu-20.04) с помощью Packer ([инструкция](https://cloud.yandex.ru/docs/tutorials/infrastructure-management/packer-quickstart)).

Чтобы получить зачёт, вам нужно предоставить скриншот страницы с созданным образом из личного кабинета YandexCloud.

---

```bash
curl -sSL https://storage.yandexcloud.net/yandexcloud-yc/install.sh | bash
exec "$BASH"
yc init
```

_[Получить токен](https://oauth.yandex.ru/authorize?response_type=token&client_id=1a6990aa636648e9b2ef855fa7bec2fb)_

_choose folder [1] default._

_configure a default Compute zone? "y"._

_choose Compute zone [1] ru-central1-a._

```bash
yc config list

yc vpc network create --name netology-net --labels class=05-virt --description "05-virt-04-docker-compose"

yc vpc subnet create --name netology-subnet-1 --zone ru-central1-a --range=10.10.10.0/24 --network-name netology-net --description "05-virt-04-docker-compose"

yc vpc network list
yc vpc network list --format yaml

yc vpc subnet list

yc compute image list --folder-id standard-images
```

```bash
sudo apt update && sudo apt install -y gnupg software-properties-common

wget -O- https://apt.releases.hashicorp.com/gpg | gpg --dearmor | sudo tee /usr/share/keyrings/hashicorp-archive-keyring.gpg

gpg --no-default-keyring --keyring /usr/share/keyrings/hashicorp-archive-keyring.gpg --fingerprint

echo "deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] https://apt.releases.hashicorp.com $(lsb_release -cs) main" | sudo tee /etc/apt/sources.list.d/hashicorp.list

sudo apt update && sudo apt install -y packer
packer plugins install github.com/hashicorp/yandex

cd netology
nano ubuntu-2004.json
```

```json
{
    "builders": [
    {
      "type": "yandex",
      "token": "y0_Ag...Zpy4",
      "folder_id": "b1ghqou3mjgcc42lmpit",
      "zone": "ru-central1-a",
      "subnet_id": "e9bbggtr94upeerf71ai",
      "use_ipv4_nat": true,
      "disk_type": "network-ssd",
      "source_image_family": "ubuntu-2004-lts",
      "image_family": "ubuntu-image",
      "image_name": "ubuntu-2004-netology",
      "image_description": "Ubuntu 20.04 LTS for Netology",
      "ssh_username": "ubuntu"
    }
    ]
}
```

```bash
packer validate ubuntu-2004.json
packer build ubuntu-2004.json

yc compute image list
```

![05-virt-04-docker-compose](05-virt-04-docker-compose.png)

---

## Задача 2

Создайте вашу первую виртуальную машину в YandexCloud с помощью web-интерфейса YandexCloud.

Чтобы получить зачёт, вам нужно предоставить страницы свойств, созданной ВМ из личного кабинета YandexCloud.

---

![05-virt-04-docker-compose-2](05-virt-04-docker-compose-2.png)

---

## Задача 3

С помощью Ansible и Docker Compose разверните на виртуальной машине в Yandex Cloud систему мониторинга на основе Prometheus/Grafana.
Используйте Ansible-код в директории ([src/ansible](https://github.com/netology-group/virt-homeworks/tree/virt-11/05-virt-04-docker-compose/src/ansible)).

Чтобы получить зачёт, вам нужно предоставить вывод команды "docker ps" , все контейнеры, описанные в [docker-compose](https://github.com/netology-group/virt-homeworks/blob/virt-11/05-virt-04-docker-compose/src/ansible/stack/docker-compose.yaml), должны быть в статусе "Up".

---

```bash
cd netology
ansible all -m ping
ansible-playbook provision.yml
```

```text
[centos@centos-7 ~]$ sudo -i
[root@centos-7 ~]# docker ps
CONTAINER ID   IMAGE   CREATED   STATUS   PORTS
83d9b22aeaa9   gcr.io/cadvisor/cadvisor:v0.47.0   Up 4 minutes (healthy)   8080/tcp
c983c3aa701c   prom/node-exporter:v0.18.1         Up 4 minutes             9100/tcp
f39ea61e768d   stefanprodan/caddy                 Up 4 minutes             0.0.0.0:3000->3000/tcp, 0.0.0.0:9090-9091->9090-9091/tcp, 0.0.0.0:9093->9093/tcp
136432cfb4f9   prom/prometheus:v2.17.1            Up 4 minutes             9090/tcp
7a30ab433d08   prom/alertmanager:v0.20.0          Up 4 minutes             9093/tcp
c13bcbd586a0   grafana/grafana:7.4.2              Up 4 minutes             3000/tcp
f2963b6c216e   prom/pushgateway:v1.2.0            Up 4 minutes             9091/tcp
```

---

## Задача 4

1. Откройте веб-браузер, зайдите на страницу http://<внешний_ip_адрес_вашей_ВМ>:3000.
2. Используйте для авторизации логин и пароль из [.env-file](https://github.com/netology-group/virt-homeworks/blob/virt-11/05-virt-04-docker-compose/src/ansible/stack/.env).
3. Изучите доступный интерфейс, найдите в интерфейсе автоматически созданные docker-compose-панели с графиками([dashboards](https://grafana.com/docs/grafana/latest/dashboards/use-dashboards/)).
4. Подождите 5-10 минут, чтобы система мониторинга успела накопить данные.

Чтобы получить зачёт, предоставьте:

- скриншот работающего веб-интерфейса Grafana с текущими метриками.

---

![05-virt-04-docker-compose-graphana](05-virt-04-docker-compose-graphana.png)

---
