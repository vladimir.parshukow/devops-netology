# Домашнее задание к занятию «Компьютерные сети. Лекция 3»

1. Подключитесь к публичному маршрутизатору в интернет. Найдите маршрут к вашему публичному IP.

    ```
    telnet route-views.routeviews.org
    Username: rviews
    show ip route x.x.x.x/32
    show bgp x.x.x.x/32
    ```

    ---

    _`curl ifconfig.me`_
    
    _178.66.157.136_

    _`show ip route 178.66.157.136`_

    ```
    Routing entry for 178.66.0.0/16
        Known via "bgp 6447", distance 20, metric 0
        Tag 3356, type external
        Last update from 4.68.4.46 5w2d ago
        Routing Descriptor Blocks:
        * 4.68.4.46, from 4.68.4.46, 5w2d ago
            Route metric is 0, traffic share count is 1
            AS Hops 2
            Route tag 3356
            MPLS label: none
    ```

    _`show bgp 178.66.157.136`_

    ```
    BGP routing table entry for 178.66.0.0/16, version 2659738399
    Paths: (20 available, best #14, table default)
        Not advertised to any peer
        Refresh Epoch 1
        8283 1299 12389, (aggregated by 12389 212.48.198.56)
            94.142.247.3 from 94.142.247.3 (94.142.247.3)
                Origin IGP, metric 0, localpref 100, valid, external
                Community: 1299:30000 8283:1 8283:101 8283:102
                unknown transitive attribute: flag 0xE0 type 0x20 length 0x24
                  value 0000 205B 0000 0000 0000 0001 0000 205B
                        0000 0005 0000 0001 0000 205B 0000 0005
                        0000 0002 
                path 7FE1077F70F8 RPKI State valid
                rx pathid: 0, tx pathid: 0
    ...
    ```

    ---

1. Создайте dummy-интерфейс в Ubuntu. Добавьте несколько статических маршрутов. Проверьте таблицу маршрутизации.

    ---

    _`sudo ip link add dummy0 type dummy`_

    _`ip -br link`_

    ```
    lo               UNKNOWN        00:00:00:00:00:00 <LOOPBACK,UP,LOWER_UP> 
    eth0             UP             08:00:27:59:cb:31 <BROADCAST,MULTICAST,UP,LOWER_UP> 
    dummy0           DOWN           1e:74:d5:bf:01:45 <BROADCAST,NOARP>
    ```

    _`sudo ip route add 172.20.20.0/24 dev eth0 metric 1`_

    _`ip route show`_

    ```
    default via 10.0.2.2 dev eth0 proto dhcp src 10.0.2.15 metric 100 
    10.0.2.0/24 dev eth0 proto kernel scope link src 10.0.2.15 
    10.0.2.2 dev eth0 proto dhcp scope link src 10.0.2.15 metric 100 
    172.20.20.0/24 dev eth0 scope link metric 1
    ```

    _`netstat -nr`_

    ```
    Kernel IP routing table
    Destination     Gateway         Genmask         Flags   MSS Window  irtt Iface
    0.0.0.0         10.0.2.2        0.0.0.0         UG        0 0          0 eth0
    10.0.2.0        0.0.0.0         255.255.255.0   U         0 0          0 eth0
    10.0.2.2        0.0.0.0         255.255.255.255 UH        0 0          0 eth0
    172.20.20.0     0.0.0.0         255.255.255.0   U         0 0          0 eth0
    ```

    _Статический маршрут в netplan (`/etc/netplan/01-netcfg.yaml`)._

    _[Источник.](https://www.dmosk.ru/miniinstruktions.php?mini=network-netplan)_

    ```
    network:
        version: 2
        renderer: networkd
        ethernets:
            ens9:
                dhcp4: no
                addresses: 192.168.1.10/24
                nameservers:
                    addresses:
                    -   8.8.8.8
                    -   77.88.8.8
                routes:
                -   to: 192.168.0.0/24
                    via: 192.168.1.1
                    on-link: true
    ```

    ---

1. Проверьте открытые TCP-порты в Ubuntu. Какие протоколы и приложения используют эти порты? Приведите несколько примеров.

    ---

    _см. следующий ответ._

    ---

1. Проверьте используемые UDP-сокеты в Ubuntu. Какие протоколы и приложения используют эти порты?

    ---

    _`ss -tunlp` (t - для tcp, u - для udp)_

    ```
    Netid   State    Recv-Q   Send-Q         Local Address:Port       Peer Address:Port
    udp     UNCONN   0        0              127.0.0.53%lo:53              0.0.0.0:*
    udp     UNCONN   0        0             10.0.2.15%eth0:68              0.0.0.0:*
    tcp     LISTEN   0        4096           127.0.0.53%lo:53              0.0.0.0:*
    tcp     LISTEN   0        128                  0.0.0.0:22              0.0.0.0:*
    tcp     LISTEN   0        128                     [::]:22                 [::]:*
    ```

    _53 (udp/tcp) – DNS_

    _68 (udp/tcp) – DHCP_

    _22 (udp/tcp) – SSH_

    _[Список портов TCP и UDP](https://ru.wikipedia.org/wiki/%D0%A1%D0%BF%D0%B8%D1%81%D0%BE%D0%BA_%D0%BF%D0%BE%D1%80%D1%82%D0%BE%D0%B2_TCP_%D0%B8_UDP)_

    ---

1. Используя diagrams.net, создайте L3-диаграмму вашей домашней сети или любой другой сети, с которой вы работали. 

    ---

    ![home-network](home-network.drawio.svg)

    ---
