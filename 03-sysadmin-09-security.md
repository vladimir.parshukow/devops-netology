# Домашнее задание к занятию «Элементы безопасности информационных систем»

1. Установите плагин Bitwarden для браузера. Зарегестрируйтесь и сохраните несколько паролей.

1. Установите Google Authenticator на мобильный телефон. Настройте вход в Bitwarden-акаунт через Google Authenticator OTP.

    ---

    ![bitwarden](bitwarden.png)

    ---

1. Установите apache2, сгенерируйте самоподписанный сертификат, настройте тестовый сайт для работы по HTTPS.

    ---

    _`sudo openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/ssl/private/apache-selfsigned.key -out /etc/ssl/certs/apache-selfsigned.crt -subj "/C=RU/ST=SPE/L=SPB/O=devops-netology/OU=Vladimir Parshukow/CN=localhost"`_

    _`sudo nano /etc/apache2/conf-available/ssl-params.conf`_

    ```
    SSLCipherSuite EECDH+AESGCM:EDH+AESGCM:AES256+EECDH:AES256+EDH
    SSLProtocol All -SSLv2 -SSLv3 -TLSv1 -TLSv1.1
    SSLHonorCipherOrder On
    # Disable preloading HSTS for now.  You can use the commented out header line that includes
    # the "preload" directive if you understand the implications.
    #Header always set Strict-Transport-Security "max-age=63072000; includeSubDomains; preload"
    Header always set X-Frame-Options DENY
    Header always set X-Content-Type-Options nosniff
    # Requires Apache >= 2.4
    SSLCompression off
    SSLUseStapling on
    SSLStaplingCache "shmcb:logs/stapling-cache(150000)"
    # Requires Apache >= 2.4.11
    SSLSessionTickets Off
    ```

    _`sudo nano /etc/apache2/sites-available/default-ssl.conf`_

    _Добавил строку: `ServerName localhost`_

    _Изменил строку: `SSLCertificateFile      /etc/ssl/certs/apache-selfsigned.crt`_

    _Изменил строку: `SSLCertificateKeyFile /etc/ssl/private/apache-selfsigned.key`_

    _Включил модуль Apache для SSL, mod_ssl, и модуль mod_headers, который необходим для работы сниппета SSL:_

    _`sudo a2enmod ssl`_

    _`sudo a2enmod headers`_

    _Включил подготовленный виртуальный хост:_

    _`sudo a2ensite default-ssl`_

    _Включил файл ssl-params.conf:_

    _`sudo a2enconf ssl-params`_

    _Проверил синтаксис на наличие ошибок:_

    _`sudo apache2ctl configtest`_

    _Перезапустил веб-сервер (это действие было лишним, т.к. я перезагружал ВМ):_

    _`sudo systemctl restart apache2`_

    _Добавил строку в конфиг vagrant: `config.vm.network "forwarded_port", guest: 443, host: 44443`_

    _Перезагрузил виртуальную машину. Зашёл на `https://localhost:44443`_

    ![apache2-ssl](apache2-ssl.png)

    _[Руководство](https://www.8host.com/blog/sozdanie-samopodpisannogo-ssl-sertifikata-dlya-apache-v-ubuntu-18-04/)_

    ---

1. Проверьте на TLS-уязвимости произвольный сайт в интернете (кроме сайтов МВД, ФСБ, МинОбр, НацБанк, РосКосмос, РосАтом, РосНАНО и любых госкомпаний, объектов КИИ, ВПК и т. п.).

    ---

    _`mkdir git && cd git && git clone --depth 1 https://github.com/drwetter/testssl.sh.git`_

    _`testssl.sh/testssl.sh https://ya.ru`_

    ```
    ###########################################################
        testssl.sh       3.2rc2 from https://testssl.sh/dev/

        This program is free software. Distribution and
                modification under GPLv2 permitted.
        USAGE w/o ANY WARRANTY. USE IT AT YOUR OWN RISK!

        Please file bugs @ https://testssl.sh/bugs/

    ###########################################################

    Using "OpenSSL 1.0.2-bad (1.0.2k-dev)" [~183 ciphers]
    on sysadm:testssl.sh/bin/openssl.Linux.x86_64
    (built: "Sep  1 14:03:44 2022", platform: "linux-x86_64")


    Testing all IPv4 addresses (port 443): 77.88.55.242 5.255.255.242
    --------------------------------------------------------------------------------
    Start 2023-02-26 16:04:57        -->> 77.88.55.242:443 (ya.ru) <<--

    Further IP addresses:   5.255.255.242 2a02:6b8::2:242 
    rDNS (77.88.55.242):    ya.ru.
    Service detected:       HTTP


    Testing protocols via sockets except NPN+ALPN 

    SSLv2      not offered (OK)
    SSLv3      not offered (OK)
    TLS 1      offered (deprecated)
    TLS 1.1    offered (deprecated)
    TLS 1.2    offered (OK)
    TLS 1.3    offered (OK): final
    NPN/SPDY   not offered
    ALPN/HTTP2 h2, http/1.1 (offered)

    Testing cipher categories 

    NULL ciphers (no encryption)                      not offered (OK)
    Anonymous NULL Ciphers (no authentication)        not offered (OK)
    Export ciphers (w/o ADH+NULL)                     not offered (OK)
    LOW: 64 Bit + DES, RC[2,4], MD5 (w/o export)      not offered (OK)
    Triple DES Ciphers / IDEA                         not offered
    Obsoleted CBC ciphers (AES, ARIA etc.)            offered
    Strong encryption (AEAD ciphers) with no FS       offered (OK)
    Forward Secrecy strong encryption (AEAD ciphers)  offered (OK)

    ...
    ```

    ---

1. Установите на Ubuntu SSH-сервер, сгенерируйте новый приватный ключ. Скопируйте свой публичный ключ на другой сервер. Подключитесь к серверу по SSH-ключу.
 
    ---

    _Установить openssh-server:_

    _`sudo apt install openssh-server`_

    _`sudo systemctl start sshd.service`_

    _`sudo systemctl enable sshd.service`_

    _Сгенерировать ключ:_

    _`ssh-keygen`_

    ```
    Generating public/private rsa key pair.
    ...
    Your identification has been saved in /home/vagrant/.ssh/id_rsa
    Your public key has been saved in /home/vagrant/.ssh/id_rsa.pub
    ...
    ```

    _Скопировать ключ:_

    _`ssh-copy-id -i .ssh/id_rsa parshukow@x.x.x.x`_

    _Подключиться по ssh:_

    _`ssh parshukow@x.x.x.x`_

    ---

1. Переименуйте файлы ключей из задания 5. Настройте файл конфигурации SSH-клиента так, чтобы вход на удалённый сервер осуществлялся по имени сервера.

    ---

    _`sudo mv ~/.ssh/id_rsa ~/.ssh/id_rsa_server`_

    _`sudo mv ~/.ssh/id_rsa.pub ~/.ssh/id_rsa_server.pub`_

    _`sudo nano ~/.ssh/config`_

    ```
    Host server
        HostName x.x.x.x
        User parshukow
        Port 22
        IdentityFile ~/.ssh/id_rsa_server
    ```

    _`ssh server`_

    ---

1. Соберите дамп трафика утилитой tcpdump в формате pcap, 100 пакетов. Откройте файл pcap в Wireshark.

    ---

    _`sudo tcpdump -c 100 -w dump.pcap`_

    ![tcpdump-for-wireshark](tcpdump-for-wireshark.png)

    ---
