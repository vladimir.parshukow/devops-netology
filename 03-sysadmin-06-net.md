# Домашнее задание к занятию "Компьютерные сети. Лекция 1"

_`sudo apt -y install telnet whois traceroute mtr net-tools`_

---

1. Работа c HTTP через телнет.
    - Подключитесь утилитой телнет к сайту stackoverflow.com `telnet stackoverflow.com 80`
    - Отправьте HTTP запрос

    ```bash
    GET /questions HTTP/1.0
    HOST: stackoverflow.com
    [press enter]
    [press enter]
    ```

    В ответе укажите полученный HTTP код, что он означает?

    ---

    ```
    Trying 151.101.65.69...
    Connected to stackoverflow.com.
    Escape character is '^]'.
    GET /questions HTTP/1.0
    HOST: stackoverflow.com

    HTTP/1.1 403 Forbidden
    Connection: close
    Content-Length: 1923
    Server: Varnish
    Retry-After: 0
    Content-Type: text/html
    Accept-Ranges: bytes
    Date: Mon, 20 Feb 2023 21:10:16 GMT
    Via: 1.1 varnish
    X-Served-By: cache-fra-eddf8230079-FRA
    X-Cache: MISS
    X-Cache-Hits: 0
    X-Timer: S1676927417.844797,VS0,VE1
    X-DNS-Prefetch-Control: off
    ```

    _Код: 403 Forbidden - доступ к запрошенному ресурсу запрещен_

    _Попробовал ещё так: `openssl s_client -connect stackoverflow.com:443`_

    ---

1. Повторите задание 1 в браузере, используя консоль разработчика F12.

    - откройте вкладку `Network`
    - отправьте запрос http://stackoverflow.com
    - найдите первый ответ HTTP сервера, откройте вкладку `Headers`
    - укажите в ответе полученный HTTP код
    - проверьте время загрузки страницы, какой запрос обрабатывался дольше всего?
    - приложите скриншот консоли браузера в ответ.

    ---

    _Код: 307 Temporary Redirect - запрошенный ресурс был временно перемещён в URL-адрес, указанный в заголовке Location_

    _Дольше всего (почти 3 секунды) обрабатывался второй запрос, это javascript._

    ![stackoverflow-1](stackoverflow-1.png)

    ---

1. Какой IP адрес у вас в интернете?

    ---

    _178.66.157.136 - посмотрел в Яндекс_

    _Полезные запросы:_
    ```
    wget -qO- eth0.me
    wget -qO- ipinfo.io/ip
    wget -qO- ipecho.net/plain
    wget -qO- icanhazip.com
    wget -qO- ipecho.net
    wget -qO- ident.me
    wget -qO- myip.gelma.net
    ```

    ---

1. Какому провайдеру принадлежит ваш IP адрес? Какой автономной системе AS? Воспользуйтесь утилитой `whois`

    ---

    _`whois 178.66.157.136 | grep descr`_

    ```
    descr:          PJSC "Rostelecom" North-West Region
    descr:          14A, Sinopskaya emb., 191167, Saint-Petersburg, Russia
    descr:          Rostelecom networks
    ```

    _`whois -h whois.radb.net 178.66.157.136 | grep origin:`_

    ```
    origin:         AS12389
    ```

    ---

1. Через какие сети проходит пакет, отправленный с вашего компьютера на адрес 8.8.8.8? Через какие AS? Воспользуйтесь утилитой `traceroute`

    ---

    _`sudo traceroute -An 8.8.8.8`_

    ```
    traceroute to 8.8.8.8 (8.8.8.8), 30 hops max, 60 byte packets
     1  10.0.2.2 [*]  0.273 ms  0.228 ms  0.197 ms
     2  192.168.0.1 [*]  16.580 ms  16.823 ms  16.864 ms
     3  100.96.64.1 [*]  19.193 ms  24.776 ms  26.081 ms
     4  100.127.1.253 [*]  26.625 ms  27.714 ms  32.903 ms
     5  212.48.195.29 [AS12389]  31.329 ms  39.383 ms  47.851 ms
     6  188.254.2.6 [AS12389]  44.619 ms  46.067 ms  45.974 ms
     7  87.226.194.47 [AS12389]  46.544 ms  50.378 ms  50.250 ms
     8  74.125.244.132 [AS15169]  51.276 ms 74.125.244.180 [AS15169]  51.704 ms  50.595 ms
     9  216.239.48.163 [AS15169]  54.079 ms 142.251.61.219 [AS15169]  56.281 ms 72.14.232.85 [AS15169]  54.723 ms
    10  142.251.51.187 [AS15169]  53.014 ms 142.251.61.221 [AS15169]  68.923 ms  59.449 ms
    11  * * 216.239.40.61 [AS15169]  28.924 ms
    12  * * *
    13  * * *
    14  * * *
    15  * * *
    16  * * *
    17  * * *
    18  * * 8.8.8.8 [AS15169/AS263411]  26.112 ms
    ```

    ---

1. Повторите задание 5 в утилите `mtr`. На каком участке наибольшая задержка - delay?

    ---

    _`mtr -zn 8.8.8.8`_

    ```
                                     Packets               Pings
     Host                          Loss%   Snt   Last   Avg  Best  Wrst StDev
     1. AS???    10.0.2.2           0.0%    23    0.9   0.9   0.7   1.1   0.1
     2. AS???    192.168.0.1        0.0%    23   10.8  27.2   3.0 138.7  28.1
     3. AS???    100.96.64.1        0.0%    23   36.6  19.6  11.4  36.6   7.1
     4. AS???    100.127.1.253      0.0%    23   16.4  30.5  13.2 140.8  34.5
     5. AS12389  212.48.195.29      0.0%    23   16.7  27.4  12.9 106.4  24.8
     6. AS12389  188.254.2.4        0.0%    23   17.3  25.2  13.9  89.2  16.9
     7. AS12389  87.226.194.47      0.0%    23   13.1  21.0  12.7  37.5   6.7
     8. AS15169  74.125.244.180     0.0%    23   18.4  29.7  16.6 151.4  27.5
     9. AS15169  216.239.48.163     0.0%    22   32.6  29.9  19.6 101.8  16.7
    10. AS15169  172.253.51.219    95.2%    22  317.1 317.1 317.1 317.1   0.0
    11. (waiting for reply)
    12. (waiting for reply)
    13. (waiting for reply)
    14. (waiting for reply)
    15. (waiting for reply)
    16. (waiting for reply)
    17. (waiting for reply)
    18. (waiting for reply)
    19. AS15169  8.8.8.8            0.0%    22   18.0  24.7  16.8  42.3   7.3
    ```

    _Участок с наибольшей задержкой: `10. AS15169  172.253.51.219    95.2%    22  317.1 317.1 317.1 317.1   0.0` - это Google_

    ---

1. Какие DNS сервера отвечают за доменное имя dns.google? Какие A записи? Воспользуйтесь утилитой `dig`

    ---

    _`dig +short NS dns.google`_

    ```
    ns2.zdns.google.
    ns1.zdns.google.
    ns3.zdns.google.
    ns4.zdns.google.
    ```

    _`dig +short A dns.google`_

    ```
    8.8.8.8
    8.8.4.4
    ```

    ---

1. Проверьте PTR записи для IP адресов из задания 7. Какое доменное имя привязано к IP? Воспользуйтесь утилитой `dig`

    ---

    _`dig -x 8.8.8.8`_

    ```
    ;; ANSWER SECTION:
    8.8.8.8.in-addr.arpa.	6419	IN	PTR	dns.google.
    ```