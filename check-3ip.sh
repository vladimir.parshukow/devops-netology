#!/usr/bin/env bash

ip=(192.168.0.21 173.194.222.113 87.250.250.242)
log=check-3ip.log
err=check-3ip.err

: >$log
: >$err
for i in {1..5}
do
    for host in ${ip[@]}
    do
        curl --connect-timeout 10 http://$host >/dev/null 2>&1
        if (($? == 0))
        then
            result="HTTP UP"
            echo $host $result >>$log
        else
            result="HTTP DOWN"
            echo $host $result >>$err
            exit 1
        fi
    done
done