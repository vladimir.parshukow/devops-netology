#!/usr/bin/env python3

import os
import sys

str_modified = '\tизменено:'   # вынесем префикс строки об изменённом файле в отдельную переменную
#showtop_command = 'git rev-parse --show-toplevel'

try:
    str_gitpath = sys.argv[1]
    os.chdir(str_gitpath)
    if str_gitpath[0] != '/':
        raise Exception
    bash_command = [f"cd {str_gitpath}", "git status"]
    is_git = os.popen('git status >/dev/null 2>&1; echo $?').read()
    if int(is_git) > 0:
        raise Exception
    result_os = os.popen(' && '.join(bash_command)).read()
except:
    print("Неправильный полный путь к репозиторию!")
    str_gitpath = '~/git/devops-netology/'
else:
    # уберём строку is_change = False
    for result in result_os.split('\n'):
        if result.find(str_modified) != -1:
            prepare_result = result.replace(str_modified, str_gitpath)   # заменим подстроку "изменено:" на каталог git
            print(prepare_result)
            # уберём break
