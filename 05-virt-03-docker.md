# Домашнее задание к занятию 3. «Введение. Экосистема. Архитектура. Жизненный цикл Docker-контейнера»

## Задача 1

Сценарий выполнения задачи:

- создайте свой репозиторий на [https://hub.docker.com](https://hub.docker.com);
- выберите любой образ, который содержит веб-сервер Nginx;
- создайте свой fork образа;
- реализуйте функциональность: запуск веб-сервера в фоне с индекс-страницей, содержащей HTML-код ниже:

```html
<html>
<head>
Hey, Netology
</head>
<body>
<h1>I’m DevOps Engineer!</h1>
</body>
</html>
```

Опубликуйте созданный fork в своём репозитории и предоставьте ответ в виде ссылки на [https://hub.docker.com/username_repo](https://hub.docker.com/username_repo).

---

```bash
sudo apt install -y docker docker.io
mkdir docker
cd docker
nano Dockerfile
```

```text
FROM nginx:1.25

MAINTAINER Vladimir Parshukow <vladimirparshukow@gmail.com>

RUN echo "daemon off;" >> /etc/nginx/nginx.conf

RUN echo "<html>" > /usr/share/nginx/html/index.html
RUN echo "<head>" >> /usr/share/nginx/html/index.html
RUN echo "Hey, Netology" >> /usr/share/nginx/html/index.html
RUN echo "</head>" >> /usr/share/nginx/html/index.html
RUN echo "<body>" >> /usr/share/nginx/html/index.html
RUN echo "<h1>I'm DevOps Engineer!</h1>" >> /usr/share/nginx/html/index.html
RUN echo "</body>" >> /usr/share/nginx/html/index.html
RUN echo "</html>" >> /usr/share/nginx/html/index.html

CMD [ "nginx" ]
```

```bash
sudo docker pull nginx:1.25
sudo docker images
sudo docker build -t vladimirparshukow/nginx:netology .
sudo docker run -d -p 8080:80 vladimirparshukow/nginx:netology
sudo docker ps
```

```text
c41abc1a20a2   vladimirparshukow/nginx:netology   "/docker-entrypoint.…"   5 seconds ago   Up 4 seconds   0.0.0.0:8080->80/tcp, :::8080->80/tcp   nice_bouman
```

```bash
curl 127.0.0.1:8080
```

```html
<html>
<head>
Hey, Netology
</head>
<body>
<h1>I'm DevOps Engineer!</h1>
</body>
</html>
```

```bash
sudo docker stop c41abc1a20a2
sudo docker ps
sudo docker login --username vladimirparshukow
sudo docker push vladimirparshukow/nginx:netology
```

_[https://hub.docker.com/r/vladimirparshukow/nginx](https://hub.docker.com/r/vladimirparshukow/nginx)_

---

## Задача 2

Посмотрите на сценарий ниже и ответьте на вопрос:
«Подходит ли в этом сценарии использование Docker-контейнеров или лучше подойдёт виртуальная машина, физическая машина? Может быть, возможны разные варианты?»

Детально опишите и обоснуйте свой выбор.

--

Сценарий:

- высоконагруженное монолитное Java веб-приложение;

---

_Для высоконагруженных приложений я бы использовал физический сервер или виртуальную машину._

---

- Nodejs веб-приложение;

---

_Docker отлично подходит для веб-приложений._

---

- мобильное приложение c версиями для Android и iOS;

---

_В мобильном приложении, похоже, нужно взаимодействие с интерфейсом, значит Докер не подойдёт, а виртуальная машина — да._

---

- шина данных на базе Apache Kafka;

---

_Apache Kafka — распределённый программный брокер сообщений, можно использовать Докер._

---

- Elasticsearch-кластер для реализации логирования продуктивного веб-приложения — три ноды elasticsearch, два logstash и две ноды kibana;

---

_Для упрощения горизонтального масштабирования Elasticsearch, лучше использовать Docker-кластер._

---

- мониторинг-стек на базе Prometheus и Grafana;

---

_Эти системы мониторинга удобно развернуть в Docker._

---

- MongoDB как основное хранилище данных для Java-приложения;

---

_Хранилище данных лучше поместить на виртуальную машину или физический сервер._

---

- Gitlab-сервер для реализации CI/CD-процессов и приватный (закрытый) Docker Registry.

---

_Сервисы можно сделать в Докере, а базу данных на виртуальную машину._

---

## Задача 3

- Запустите первый контейнер из образа **centos** c любым тегом в фоновом режиме, подключив папку ```/data``` из текущей рабочей директории на хостовой машине в ```/data``` контейнера.
- Запустите второй контейнер из образа **debian** в фоновом режиме, подключив папку ```/data``` из текущей рабочей директории на хостовой машине в ```/data``` контейнера.
- Подключитесь к первому контейнеру с помощью ```docker exec``` и создайте текстовый файл любого содержания в ```/data```.
- Добавьте ещё один файл в папку ```/data``` на хостовой машине.
- Подключитесь во второй контейнер и отобразите листинг и содержание файлов в ```/data``` контейнера.

---

```bash
mkdir data
sudo docker run -v ~/docker/data/:/data -it -d centos
sudo docker run -v ~/docker/data/:/data -it -d debian
sudo docker ps
sudo docker exec -it optimistic_aryabhata /bin/bash

cd data
echo "text" > test
exit

cd data
echo "empty" > newfile
cd ..

sudo docker exec -it crazy_bhabha /bin/bash

ls
cat test
cat newfile
exit
```

![05-virt-03-docker](05-virt-03-docker.png)

---
