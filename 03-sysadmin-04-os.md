# Домашнее задание к занятию "Операционные системы. Лекция 2"

1. На лекции мы познакомились с [node_exporter](https://github.com/prometheus/node_exporter/releases). В демонстрации его исполняемый файл запускался в background. Этого достаточно для демо, но не для настоящей production-системы, где процессы должны находиться под внешним управлением. Используя знания из лекции по systemd, создайте самостоятельно простой [unit-файл](https://www.freedesktop.org/software/systemd/man/systemd.service.html) для node_exporter:

    * поместите его в автозагрузку,
    * предусмотрите возможность добавления опций к запускаемому процессу через внешний файл (посмотрите, например, на `systemctl cat cron`),
    * удостоверьтесь, что с помощью systemctl процесс корректно стартует, завершается, а после перезагрузки автоматически поднимается.

    ---

    _`wget https://github.com/prometheus/node_exporter/releases/download/v1.5.0/node_exporter-1.5.0.linux-amd64.tar.gz`_

    _`tar xzf node_exporter-*.linux-amd64.tar.gz`_

    _`sudo mv node_exporter-*.linux-amd64/node_exporter /usr/bin/`_

    _`sudo nano /etc/systemd/system/node_exporter.service`_
    ```
    [Unit]
    Description=Prometheus exporter for machine metrics
    Documentation=https://github.com/prometheus/node_exporter                      

    [Service]
    Restart=always
    EnvironmentFile=/etc/default/node_exporter
    ExecStart=/usr/bin/node_exporter $ARGS
    ExecReload=/bin/kill -HUP $MAINPID
    TimeoutStopSec=20s
    SendSIGKILL=no

    [Install]
    WantedBy=multi-user.target
    ```

    _`systemctl daemon-reload`_

    _`sudo systemctl enable node_exporter.service`_

    _`systemctl restart node_exporter`_

    _`systemctl status node_exporter`_

    _Дополнительные опции через внешний файл реализуются с помощью строки `EnvironmentFile=...` и переменной `$ARGS`._

    ---

1. Ознакомьтесь с опциями node_exporter и выводом `/metrics` по-умолчанию. Приведите несколько опций, которые вы бы выбрали для базового мониторинга хоста по CPU, памяти, диску и сети.

    ---

    ```
    node_cpu_seconds_total{cpu="0",mode="system"} 8.54
    node_cpu_seconds_total{cpu="0",mode="user"} 11.65
    node_cpu_seconds_total{cpu="1",mode="system"} 8.12
    node_cpu_seconds_total{cpu="1",mode="user"} 10.37
    node_memory_MemAvailable_bytes 7.09566464e+08
    node_memory_MemFree_bytes 2.98504192e+08
    node_memory_MemTotal_bytes 1.024110592e+09
    node_filesystem_avail_bytes{device="/dev/sda2",fstype="ext4",mountpoint="/boot"} 1.805344768e+09
    node_filesystem_size_bytes{device="/dev/sda2",fstype="ext4",mountpoint="/boot"} 2.040373248e+09
    node_disk_io_now{device="sda"} 0
    node_network_receive_bytes_total{device="eth0"} 5.035525e+06
    node_network_receive_errs_total{device="eth0"} 0
    node_network_transmit_bytes_total{device="eth0"} 343180
    node_network_transmit_errs_total{device="eth0"} 0
    ```

    ---

1. Установите в свою виртуальную машину [Netdata](https://github.com/netdata/netdata). Воспользуйтесь [готовыми пакетами](https://packagecloud.io/netdata/netdata/install) для установки (`sudo apt install -y netdata`). 
   
   После успешной установки:
    * в конфигурационном файле `/etc/netdata/netdata.conf` в секции [web] замените значение с localhost на `bind to = 0.0.0.0`,
    * добавьте в Vagrantfile проброс порта Netdata на свой локальный компьютер и сделайте `vagrant reload`:

    ```bash
    config.vm.network "forwarded_port", guest: 19999, host: 19999
    ```

    После успешной перезагрузки в браузере *на своем ПК* (не в виртуальной машине) вы должны суметь зайти на `localhost:19999`. Ознакомьтесь с метриками, которые по умолчанию собираются Netdata и с комментариями, которые даны к этим метрикам.

    ---

    ![netdata-screenshot-1](netdata-screenshot-1.png)

    ---

1. Можно ли по выводу `dmesg` понять, осознает ли ОС, что загружена не на настоящем оборудовании, а на системе виртуализации?

    ---

    _Похоже что осознаёт, одно из первых сообщений `dmesg` : `[    0.000000] Hypervisor detected: KVM`_

    ---

1. Как настроен sysctl `fs.nr_open` на системе по-умолчанию? Определите, что означает этот параметр. Какой другой существующий лимит не позволит достичь такого числа (`ulimit --help`)?

    ---

    _"fs.nr_open" - это максимальное число открытых дескрипторов. Существует "soft" лимит `ulimit -Sn`, который может быть изменён, но не более "hard" лимита `ulimit -Hn`, который, в свою очередь, не может превышать "fs.nr_open"._

    ---

1. Запустите любой долгоживущий процесс (не `ls`, который отработает мгновенно, а, например, `sleep 1h`) в отдельном неймспейсе процессов; покажите, что ваш процесс работает под PID 1 через `nsenter`. Для простоты работайте в данном задании под root (`sudo -i`). Под обычным пользователем требуются дополнительные опции (`--map-root-user`) и т.д.

    ---

    _`unshare -f --pid --mount-proc sleep 1h`_

    _Откроем другой терминал, `vagrant ssh`, найдём процесс "sleep": `ps ax | grep sleep`_

    ```
    1709 pts/0    S+     0:00 unshare -f --pid --mount-proc sleep 1h
    1710 pts/0    S+     0:00 sleep 1h
    1789 pts/1    S+     0:00 grep --color=auto sleep
    ```

    _`nsenter -t 1710 -p -r ps ax`_

    ```
    PID TTY      STAT   TIME COMMAND
      1 pts/0    S+     0:00 sleep 1h
      6 pts/1    R+     0:00 ps ax
    ```

    ---

1. Найдите информацию о том, что такое `:(){ :|:& };:`. Запустите эту команду в своей виртуальной машине Vagrant с Ubuntu 20.04 (**это важно, поведение в других ОС не проверялось**). Некоторое время все будет "плохо", после чего (минуты) – ОС должна стабилизироваться. Вызов `dmesg` расскажет, какой механизм помог автоматической стабилизации.  
Как настроен этот механизм по-умолчанию, и как изменить число процессов, которое можно создать в сессии?

    ---

    _Функция `:(){ :|:& };:` рекурсивно создаёт две свои копии._

    _`dmesg`_
    ```
    [ 2799.040150] cgroup: fork rejected by pids controller in /user.slice/user-1000.slice/session-4.scope
    ```

    _Выход из рекурсии произошёл из-за превышения ограничений по умолчанию `ulimit -a`. Число процессов, которые можно создать в сессии, выставляется с помощью `ulimit -u`._

    ---
