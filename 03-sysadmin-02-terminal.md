# Домашнее задание к занятию "Работа в терминале. Лекция 2"

1. Какого типа команда `cd`? Попробуйте объяснить, почему она именно такого типа: опишите ход своих мыслей, если считаете, что она могла бы быть другого типа.

    _Команда `cd` ‒ является встроенной командой оболочки, потому что она влияет только на текущую оболочку, в которой она запущена._

1. Какая альтернатива без pipe команде `grep <some_string> <some_file> | wc -l`?   

    <details>
    <summary>Подсказка</summary>

    `man grep` поможет в ответе на этот вопрос. 

    </details>
    
    Ознакомьтесь с [документом](http://www.smallo.ruhr.de/award.html) о других подобных некорректных вариантах использования pipe.

    _`grep --count <some_string> <some_file>`_

1. Какой процесс с PID `1` является родителем для всех процессов в вашей виртуальной машине Ubuntu 20.04?

    _Вводим `ps aux`, и видим, что процесс с PID 1 запущен командой `/sbin/init`_

    ```bash
    $ ps aux
    USER         PID %CPU %MEM    VSZ   RSS TTY      STAT START   TIME COMMAND
    root           1  4.0  1.1 102604 11324 ?        Ss   22:29   0:01 /sbin/init
    ```

    _Теперь мы можем проверить является ли `/sbin/init` символьной ссылкой, с помощью `ls -l /sbin/init`,  выясняем, что это символьная ссылка на файл `/lib/systemd/systemd`._

1. Как будет выглядеть команда, которая перенаправит вывод stderr `ls` на другую сессию терминала?

    _Узнаем идентификатор pts другой сессии терминала, для этого введём в этой сессии команду `tty`. Далее в первом терминале вводим, например, `ls /root/ 2> /dev/pts/2`, и получаем вывод в другом терминале: `ls: невозможно открыть каталог '/root/': Отказано в доступе`._

1. Получится ли одновременно передать команде файл на stdin и вывести ее stdout в другой файл? Приведите работающий пример.

    _`cat <file1 >file2`_

1. Получится ли, находясь в графическом режиме, вывести данные из PTY в какой-либо из эмуляторов TTY? Сможете ли вы наблюдать выводимые данные?

    _Если открыть созданную ранее виртуальную машину Vagrant через интерфейс VirtualBox, то можно считать данную консоль как TTY. Логинимся, вводим команду `tty` и получаем `/dev/tty1`. Теперь из программы терминала хоста (который является PTY) подключаемся к машине по ssh: `vagrant ssh`. Пробуем перенаправить вывод: `echo Hello tty >/dev/tty1`. Текст `Hello tty` появился в консоли._

1. Выполните команду `bash 5>&1`. К чему она приведет? Что будет, если вы выполните `echo netology > /proc/$$/fd/5`? Почему так происходит?

    _Команда `bash 5>&1` создаёт новый дескриптор потока №5 и направляет его в /dev/pts/X (если дескриптор потока 1 указывал туда). `echo netology > /proc/$$/fd/5` перенаправляет вывод `echo` в дескриптор №5 текущей ($$) оболочки.

1. Получится ли в качестве входного потока для pipe использовать только stderr команды, не потеряв при этом отображение stdout на pty?  
    Напоминаем: по умолчанию через pipe передается только stdout команды слева от `|` на stdin команды справа.
Это можно сделать, поменяв стандартные потоки местами через промежуточный новый дескриптор, который вы научились создавать в предыдущем вопросе.

    _`ls {/home,/root} 3>&2 2>&1 1>&3 | sed 's/t/@/'`_

1. Что выведет команда `cat /proc/$$/environ`? Как еще можно получить аналогичный по содержанию вывод?

    _Команда выведет список переменных окружения, как и команда `env`_

1. Используя `man`, опишите что доступно по адресам `/proc/<PID>/cmdline`, `/proc/<PID>/exe`.

    _Смотрим `man proc`, находим `cmdline` с помощью `/`._

    _`/proc/<PID>/cmdline` ‒ это файл, содержащий командную строку, с помощью которой запущен процесс._

    _`/proc/<PID>/exe` ‒ это символьная ссылка, указывающая на исполняемый файл процесса._

1. Узнайте, какую наиболее старшую версию набора инструкций SSE поддерживает ваш процессор с помощью `/proc/cpuinfo`.

    _`cat /proc/cpuinfo | grep sse`. Старшая версия sse4.2_

1. При открытии нового окна терминала и `vagrant ssh` создается новая сессия и выделяется pty.  
    Это можно подтвердить командой `tty`, которая упоминалась в лекции 3.2.  
    Однако:

    ```bash
    vagrant@netology1:~$ ssh localhost 'tty'
    not a tty
    ```

    Почитайте, почему так происходит, и как изменить поведение.

    _Это происходит из-за того, что при выполнении такой команды не открывается псевдотерминал. Если же подключиться `ssh localhost`, то псевдотерминал будет открыт и ожидать ввода, тогда `tty` покажет идентификатор pts. Также можно поставить флаг `-t` (Force pseudo-terminal allocation): `ssh -t localhost 'tty'`_ 

1. Бывает, что есть необходимость переместить запущенный процесс из одной сессии в другую. Попробуйте сделать это, воспользовавшись `reptyr`. Например, так можно перенести в `screen` процесс, который вы запустили по ошибке в обычной SSH-сессии.

    _Воспольуемся [руководством reptyr](https://github.com/nelhage/reptyr#typical-usage-pattern). Сначала отредактируем файл `/etc/sysctl.d/10-ptrace.conf`, зададим значение: `kernel.yama.ptrace_scope = 0`, чтобы разрешить перехват процессов. Перезапустим vagrant._
    
    _Подключимся к виртуальной машине `vagrant ssh`. Запустим процесс `top`. Отправим в фон с помощью CTRL+z. Запустим команду `jobs -l`, чтобы узнать его номер задания [1] и PID **1811**. Запустим в фоне командой `bg %1`. Откроем новый screen: `screen -s thescreen`. Перенесём процесс `reptyr 1811`. Отсоединимся от screen, нажав сочетание CTRL+a, затем сразу клавишу d без CTRL. Закроем ssh: `exit`._

    _Теперь откроем экран виртуальной машины в интерфейсе VirtualBox и залогинимся. `screen -ls` покажет список сессий screen. Откроем нашу сессию `screen -r thescreen` и увидим там работающий top._

1. `sudo echo string > /root/new_file` не даст выполнить перенаправление под обычным пользователем, так как перенаправлением занимается процесс shell'а, который запущен без `sudo` под вашим пользователем. Для решения данной проблемы можно использовать конструкцию `echo string | sudo tee /root/new_file`. Узнайте? что делает команда `tee` и почему в отличие от `sudo echo` команда с `sudo tee` будет работать.

    _Команда `tee` направляет вывод и в файл, и в stdout одновременно. Во второй конструкции повышение прав происходит после перенаправления, поэтому конструкция работает._

# Шпаргалки для себя.

Файлы открытые процессом

```bash
$ lsof -p <PID>
```

Список файлов открытых текущим терминалом

```bash
$ lsof -p $$
```

```bash
$ ls -l /proc/$$/fd/{0,1,2}
```

Перенаправление потоков.

Пример: перенаправить stdout и stderr в /tmp/log

`>/tmp/log 2>&1`

Здесь первая часть перенапрваляет **1** в `/tmp/log`. А затем перенаправляет **2** туда же, куда сейчас указывает дескриптор потока **1**.

Поэтому запись `2>&1 >/tmp/log` будет работать не так, как мы хотим. Тут **2** перенаправит в stdout, туда в данный момент указывает дескриптор **1**. А затем **1** перенаправит в `/tmp/log`.

Перенаправление команды sed в тот же файл

`sed 's/foo/bar/' file > file` ‒ результат: файл пуст

`sed 's/foo/bar/' file >> file` ‒ результат: к файлу добавилось содержимое файла с заменой

`sed -u 's/foo/bar/' file > file` ‒ результат: файл пуст

`sed -u 's/foo/bar/' file >> file` ‒ результат: бесконечное добавление содержимого файла с заменой в конец файла
