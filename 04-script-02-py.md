# Домашнее задание к занятию «Использование Python для решения типовых DevOps-задач»

## Задание 1

Есть скрипт:

```python
#!/usr/bin/env python3
a = 1
b = '2'
c = a + b
```

### Вопросы:

| Вопрос  | Ответ |
| ------------- | ------------- |
| Какое значение будет присвоено переменной `c`?  | никакое, `'c' is not defined`  |
| Как получить для переменной `c` значение 12?  | c = f"{a}" + b  |
|   | c = str(a) + b  |
| Как получить для переменной `c` значение 3?  | c = a + int(b)  |

------

## Задание 2

Мы устроились на работу в компанию, где раньше уже был DevOps-инженер. Он написал скрипт, позволяющий узнать, какие файлы модифицированы в репозитории относительно локальных изменений. Этим скриптом недовольно начальство, потому что в его выводе есть не все изменённые файлы, а также непонятен полный путь к директории, где они находятся. 

Как можно доработать скрипт ниже, чтобы он исполнял требования вашего руководителя?

```python
#!/usr/bin/env python3

import os

bash_command = ["cd ~/netology/sysadm-homeworks", "git status"]
result_os = os.popen(' && '.join(bash_command)).read()
is_change = False
for result in result_os.split('\n'):
    if result.find('modified') != -1:
        prepare_result = result.replace('\tmodified:   ', '')
        print(prepare_result)
        break
```

### Ваш скрипт:

```python
#!/usr/bin/env python3

import os

str_modified = '\tизменено:'   # вынесем префикс строки об изменённом файле в отдельную переменную
# полезная команда: git rev-parse --show-toplevel

str_gitpath = '~/git/devops-netology/'
bash_command = [f"cd {str_gitpath}", "git status"]
result_os = os.popen(' && '.join(bash_command)).read()
# уберём строку is_change = False
for result in result_os.split('\n'):
    if result.find(str_modified) != -1:
        prepare_result = result.replace(str_modified, str_gitpath)   # заменим подстроку "изменено:" на каталог git
        print(prepare_result)
        # уберём break
```

### Вывод скрипта при запуске во время тестирования:

```
~/git/devops-netology/      04-script-02-py.md
~/git/devops-netology/      search-modified.py
~/git/devops-netology/      terraform/test
```

------

## Задание 3

Доработать скрипт выше так, чтобы он не только мог проверять локальный репозиторий в текущей директории, но и умел воспринимать путь к репозиторию, который мы передаём, как входной параметр. Мы точно знаем, что начальство будет проверять работу этого скрипта в директориях, которые не являются локальными репозиториями.

### Ваш скрипт:

```python
#!/usr/bin/env python3

import os
import sys

str_modified = '\tизменено:'   # вынесем префикс строки об изменённом файле в отдельную переменную
# полезная команда: git rev-parse --show-toplevel

try:
    str_gitpath = sys.argv[1]
    os.chdir(str_gitpath)
    if str_gitpath[0] != '/':
        raise Exception
    bash_command = [f"cd {str_gitpath}", "git status"]
    is_git = os.popen('git status >/dev/null 2>&1; echo $?').read()
    if int(is_git) > 0:
        raise Exception
    result_os = os.popen(' && '.join(bash_command)).read()
except:
    print("Неправильный полный путь к репозиторию!")
else:
    # уберём строку is_change = False
    for result in result_os.split('\n'):
        if result.find(str_modified) != -1:
            prepare_result = result.replace(str_modified, str_gitpath)   # заменим подстроку "изменено:" на каталог git
            print(prepare_result)
            # уберём break
```

### Вывод скрипта при запуске во время тестирования:

```
~/git/devops-netology/      04-script-02-py.md
~/git/devops-netology/      search-modified.py
~/git/devops-netology/      terraform/test
```

### Вывод скрипта при запуске во время тестирования (при указании не локального репозитория):

```
Неправильный полный путь к репозиторию!
```

------

## Задание 4

Наша команда разрабатывает несколько веб-сервисов, доступных по HTTPS. Мы точно знаем, что на их стенде нет никакой балансировки, кластеризации, за DNS прячется конкретный IP сервера, где установлен сервис. 

Проблема в том, что отдел, занимающийся нашей инфраструктурой, очень часто меняет нам сервера, поэтому IP меняются примерно раз в неделю, при этом сервисы сохраняют за собой DNS-имена. Это бы совсем никого не беспокоило, если бы несколько раз сервера не уезжали в такой сегмент сети нашей компании, который недоступен для разработчиков. 

Мы хотим написать скрипт, который: 

- опрашивает веб-сервисы; 
- получает их IP; 
- выводит информацию в стандартный вывод в виде: <URL сервиса> - <его IP>. 

Также должна быть реализована возможность проверки текущего IP сервиса c его IP из предыдущей проверки. Если проверка будет провалена — оповестить об этом в стандартный вывод сообщением: [ERROR] <URL сервиса> IP mismatch: <старый IP> <Новый IP>. Будем считать, что наша разработка реализовала сервисы: `drive.google.com`, `mail.google.com`, `google.com`.

### Ваш скрипт:

```python
#!/usr/bin/env python3

import socket
import time

services = {"drive.google.com":"" ,"mail.google.com":"" ,"google.com":""}
i = 1
while True:
    for host ,ip in services.items():
        newip = socket.gethostbyname(host)
        services[host] = newip
        if ip == "":
            print(f"{host} - {newip}")
        elif newip != ip:
            print(f"[ERROR] {host} IP mismatch: {ip} {newip}")
        else:
            print(f"{host} - {newip}")
    time.sleep(1)
```

### Вывод скрипта при запуске во время тестирования:

```
drive.google.com - 64.233.163.194
mail.google.com - 142.250.74.165
google.com - 142.250.74.110
drive.google.com - 64.233.163.194
mail.google.com - 142.250.74.165
google.com - 142.250.74.110
drive.google.com - 64.233.163.194
mail.google.com - 142.250.74.165
[ERROR] google.com IP mismatch: 142.250.74.110 192.168.0.2
drive.google.com - 64.233.163.194
mail.google.com - 142.250.74.165
google.com - 192.168.0.2
```

_`echo 192.168.0.2 google.com | sudo tee -a /etc/hosts`_