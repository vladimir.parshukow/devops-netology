# Домашнее задание к занятию «Языки разметки JSON и YAML»

## Задание 1

Мы выгрузили JSON, который получили через API-запрос к нашему сервису:

```
    { "info" : "Sample JSON output from our service\t",
        "elements" :[
            { "name" : "first",
            "type" : "server",
            "ip" : 7175 
            }
            { "name" : "second",
            "type" : "proxy",
            "ip : 71.78.22.43
            }
        ]
    }
```
  Нужно найти и исправить все ошибки, которые допускает наш сервис.

### Ваш скрипт:

```json
{ "info" : "Sample JSON output from our service\t",
    "elements" :[
        { "name" : "first",
        "type" : "server",
        "ip" : "7.1.7.5" 
        }
       ,{ "name" : "second",
        "type" : "proxy",
        "ip" : "71.78.22.43"
        }
    ]
}
```

_Пропущены кавычки после ip и вокруг адреса 71.78.22.43 во втором объекте {...}_

_Пропущена запятая между двумя объектами {...} {...}_

_Пробелы между лексемами не являются обязательными_

_Остальное не является ошибками формата JSON, но, очевидно, что 7175 не является валидным ip-адресом. Простой способ превратить этот набор цифр в ip-адрес – проставить точки =) и заключить в кавычки_

---

## Задание 2

В прошлый рабочий день мы создавали скрипт, позволяющий опрашивать веб-сервисы и получать их IP. К уже реализованному функционалу нам нужно добавить возможность записи JSON и YAML-файлов, описывающих наши сервисы. 

Формат записи JSON по одному сервису: `{ "имя сервиса" : "его IP" }`. 

Формат записи YAML по одному сервису: `- имя сервиса: его IP`. 

Если в момент исполнения скрипта меняется IP у сервиса — он должен так же поменяться в YAML и JSON-файле.

### Ваш скрипт:

```python
#!/usr/bin/env python3

import os
import socket
import json
import yaml
import time

os.chdir(os.path.dirname(__file__))

services = [ {"drive.google.com":""} ,{"mail.google.com":""} ,{"google.com":""} ]
i = 1
while True:
    for i in range(3):
        host = list(services[i].keys())[0]
        ip = services[i][host]
        newip = socket.gethostbyname(host)
        services[i][host] = newip
        if ip == "":
            print(f"{host} - {newip}")
        elif newip != ip:
            print(f"[ERROR] {host} IP mismatch: {ip} {newip}")
        else:
            print(f"{host} - {newip}")
    with open('services.json', 'w') as services_json:
        services_json.write(json.dumps(services, indent=4))
    with open('services.yml', 'w') as services_yaml:
        services_yaml.write(yaml.dump(services, indent=4, explicit_start=True, explicit_end=True))
    time.sleep(1)
```

### Вывод скрипта при запуске во время тестирования:

```
drive.google.com - 64.233.163.194
mail.google.com - 142.250.74.165
google.com - 142.250.74.110
```

### JSON-файл(ы), который(е) записал ваш скрипт:

```json
[
    {
        "drive.google.com": "64.233.163.194"
    },
    {
        "mail.google.com": "142.250.74.165"
    },
    {
        "google.com": "142.250.74.110"
    }
]
```

### YAML-файл(ы), который(е) записал ваш скрипт:

```yaml
---
-   drive.google.com: 64.233.163.194
-   mail.google.com: 142.250.74.165
-   google.com: 142.250.74.110
...
```