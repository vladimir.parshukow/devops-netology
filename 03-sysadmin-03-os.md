# Домашнее задание к занятию "Операционные системы. Лекция 1"

1. Какой системный вызов делает команда `cd`? 

    В прошлом ДЗ мы выяснили, что `cd` не является самостоятельной  программой, это `shell builtin`, поэтому запустить `strace` непосредственно на `cd` не получится. Тем не менее, вы можете запустить `strace` на `/bin/bash -c 'cd /tmp'`. В этом случае вы увидите полный список системных вызовов, которые делает сам `bash` при старте. 

    Вам нужно найти тот единственный, который относится именно к `cd`. Обратите внимание, что `strace` выдаёт результат своей работы в поток stderr, а не в stdout.

    ---

    _`strace bash -c 'cd /tmp' 2>&1 | grep /tmp`_

    ```
    execve("/usr/bin/bash", ["bash", "-c", "cd /tmp"], 0x7ffc21ff4020 /* 23 vars */) = 0
    stat("/tmp", {st_mode=S_IFDIR|S_ISVTX|0777, st_size=4096, ...}) = 0
    chdir("/tmp")                           = 0
    ```

    _Ответ: `chdir("/tmp")`_

    ---

1. Попробуйте использовать команду `file` на объекты разных типов в файловой системе. Например:

    ```bash
    vagrant@netology1:~$ file /dev/tty
    /dev/tty: character special (5/0)
    vagrant@netology1:~$ file /dev/sda
    /dev/sda: block special (8/0)
    vagrant@netology1:~$ file /bin/bash
    /bin/bash: ELF 64-bit LSB shared object, x86-64
    ```

    Используя `strace` выясните, где находится база данных `file`, на основании которой она делает свои догадки.

    ---

    _`man file` подсказывает, что команда `file` ищет "магическо число" в файле с именем `?magic*`, который может находиться в разных директориях._

    _Я отфильтровал по слову "magic" вызовы при использовании команд `file /dev/tty`, `file /dev/sda`, `file /bin/bash`: например, `strace file /dev/tty 2>&1 | grep "[./]magic"`. Получается один и тот же список строк:_

    ```
    stat("/home/vagrant/.magic.mgc", 0x7ffe08e6ace0) = -1 ENOENT (No such file or directory)
    stat("/home/vagrant/.magic", 0x7ffe08e6ace0) = -1 ENOENT (No such file or directory)
    openat(AT_FDCWD, "/etc/magic.mgc", O_RDONLY) = -1 ENOENT (No such file or directory)
    stat("/etc/magic", {st_mode=S_IFREG|0644, st_size=111, ...}) = 0
    openat(AT_FDCWD, "/etc/magic", O_RDONLY) = 3
    openat(AT_FDCWD, "/usr/share/misc/magic.mgc", O_RDONLY) = 3
    ```

    _При этом первые три вызова возвращают результат "No such file or directory", значит база данных находится в файлах: `/etc/magic`, `/usr/share/misc/magic.mgc`._

    ---

1. Предположим, приложение пишет лог в текстовый файл. Этот файл оказался удален (deleted в lsof), однако возможности сигналом сказать приложению переоткрыть файлы или просто перезапустить приложение – нет. Так как приложение продолжает писать в удаленный файл, место на диске постепенно заканчивается. Основываясь на знаниях о перенаправлении потоков предложите способ обнуления открытого удаленного файла (чтобы освободить место на файловой системе).

    ---

    _Запустим непрерывное задание в фоне:_
    
    _logwriter.py_
    ```python
    #!/usr/bin/env python3
    # -*- coding: utf-8 -*-

    log = open('big.log', 'w')
    while True:
        log.write('1')
    log.close()
    ```
    _`python3 logwriter.py &`_
    
    _Удалим файл `rm big.log`, но файл не удалён и продолжает расти, т.к. занят процессом. Узнаем PID и дескриптор потока `lsof | grep big.log`. Обнулим файл, чтобы освободить место `cat /dev/null > /proc/<PID>/fd/<descr>`._

    ---

1. Занимают ли зомби-процессы какие-то ресурсы в ОС (CPU, RAM, IO)?

    ---

    _Не занимают._

    ---

1. В iovisor BCC есть утилита `opensnoop`:

    ```bash
    root@vagrant:~# dpkg -L bpfcc-tools | grep sbin/opensnoop
    /usr/sbin/opensnoop-bpfcc
    ```

    На какие файлы вы увидели вызовы группы `open` за первую секунду работы утилиты? Воспользуйтесь пакетом `bpfcc-tools` для Ubuntu 20.04. Дополнительные [сведения по установке](https://github.com/iovisor/bcc/blob/master/INSTALL.md).

    ---

    _`timeout 1 strace opensnoop-bpfcc 2>&1 | grep openat`_

    ---

1. Какой системный вызов использует `uname -a`? Приведите цитату из man по этому системному вызову, где описывается альтернативное местоположение в `/proc`, где можно узнать версию ядра и релиз ОС.

    ---

    _`uname -a`_

    ```
    Linux vagrant 5.4.0-135-generic #152-Ubuntu SMP Wed Nov 23 20:19:22 UTC 2022 x86_64 x86_64 x86_64 GNU/Linux
    ```

    _`strace uname -a 2>&1 | grep Linux | grep vagrant`_

    ```
    uname({sysname="Linux", nodename="vagrant", ...}) = 0
    uname({sysname="Linux", nodename="vagrant", ...}) = 0
    uname({sysname="Linux", nodename="vagrant", ...}) = 0
    write(1, "Linux vagrant 5.4.0-135-generic "..., 108Linux vagrant 5.4.0-135-generic #152-Ubuntu SMP Wed Nov 23 20:19:22 UTC 2022 x86_64 x86_64 x86_64 GNU/Linux
    ```

    _Установим "manpages": `sudo apt install manpages-dev`_

    _Откроем раздел 2 (системные вызовы), вызов "uname": `man 2 uname`. Найдём `/proc`._

    ```
    Part of the utsname information is also accessible via /proc/sys/kernel/{ostype, hostname, osrelease, version, domainname}.
    ```

    ---

1. Чем отличается последовательность команд через `;` и через `&&` в bash? Например:

    ```bash
    root@netology1:~# test -d /tmp/some_dir; echo Hi
    Hi
    root@netology1:~# test -d /tmp/some_dir && echo Hi
    root@netology1:~#
    ```

    Есть ли смысл использовать в bash `&&`, если применить `set -e`?

    ---

    _Точка с запятой служит для разделения команд, записанных в одну строку. Следующая команда выполняется независимо от результата предыдущей._

    _Двойной амперсанд `&&` используется в том случае, если команда следующая за ним должна быть выполнена, только если предыдущая была выполнена успешно._

    _Команда `set -e`, заставляет оболочку завершить работу, если одна из следующих команд была выполнена с ошибкой, за исключением: команд в конвейере; команд в циклах и условиях; команды в списке, разделённом `||` `&&`, кроме последней. Двойной амперсанд `&&` в этом случае можно использовать, чтобы избежать завершения выполнения после ошибочной команды и выполнить другую._

    ---

1. Из каких опций состоит режим bash `set -euxo pipefail` и почему его хорошо было бы использовать в сценариях?

    ---

    _`-e` : Немедленный выход, если команда завершается с ненулевым статусом._

    _`-u` : Проверяет инициализацию переменных, если переменной не будет, то немедленно завершится._

    _`-x` : Очень полезен при отладке. С помощью него bash печатает в стандартный вывод все команды перед их исполнением._

    _`-o pipefail` : Если нужно убедиться, что не только последняя, а все команды в пайпах завершились успешно. Но завершение работы произойдёт после конвейера, а не во время._

    _Этот режим хорошо использовать при отладке и тестировании сценария, а также для предотвращения неправильного поведения сценария, если произошла непредвиденная ошибка._

    ---

1. Используя `-o stat` для `ps`, определите, какой наиболее часто встречающийся статус у процессов в системе. В `man ps` ознакомьтесь (`/PROCESS STATE CODES`) что значат дополнительные к основной заглавной буквы статуса процессов. Его можно не учитывать при расчете (считать S, Ss или Ssl равнозначными).

    ---

    `ps -A -o stat | cut -c 1 | sort | uniq -c | sort -h`

    ```
          1 R
         46 I
         63 S
    ```

    `man ps` , `/PROCESS STATE CODES` :

    ```
    PROCESS STATE CODES
        Here are the different values that the s, stat and state output specifiers (header "STAT" or "S")
        will display to describe the state of a process:

            D    uninterruptible sleep (usually IO)
            I    Idle kernel thread
            R    running or runnable (on run queue)
            S    interruptible sleep (waiting for an event to complete)
            T    stopped by job control signal
            t    stopped by debugger during the tracing
            W    paging (not valid since the 2.6.xx kernel)
            X    dead (should never be seen)
            Z    defunct ("zombie") process, terminated but not reaped by its parent

        For BSD formats and when the stat keyword is used, additional characters may be displayed:

            <    high-priority (not nice to other users)
            N    low-priority (nice to other users)
            L    has pages locked into memory (for real-time and custom IO)
            s    is a session leader
            l    is multi-threaded (using CLONE_THREAD, like NPTL pthreads do)
            +    is in the foreground process group
    ```
    ---




    