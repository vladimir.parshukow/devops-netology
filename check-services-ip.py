#!/usr/bin/env python3

import os
import socket
import json
import yaml
import time

os.chdir(os.path.dirname(__file__))

services = [ {"drive.google.com":""} ,{"mail.google.com":""} ,{"google.com":""} ]
i = 1
while True:
    for i in range(3):
        host = list(services[i].keys())[0]
        ip = services[i][host]
        newip = socket.gethostbyname(host)
        services[i][host] = newip
        if ip == "":
            print(f"{host} - {newip}")
        elif newip != ip:
            print(f"[ERROR] {host} IP mismatch: {ip} {newip}")
        else:
            print(f"{host} - {newip}")
    with open('services.json', 'w') as services_json:
        services_json.write(json.dumps(services, indent=4))
    with open('services.yml', 'w') as services_yaml:
        services_yaml.write(yaml.dump(services, indent=4, explicit_start=True, explicit_end=True))
    time.sleep(1)
