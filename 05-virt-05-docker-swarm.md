# Домашнее задание к занятию 5. «Оркестрация кластером Docker контейнеров на примере Docker Swarm»

## Задача 1

Дайте письменные ответы на вопросы:

- В чём отличие режимов работы сервисов в Docker Swarm-кластере: replication и global?

---

_В режиме replication пользователь задаёт количество реплик, но кластер сам решает на каких нодах они будут развёрнуты, можно лишь указать максимальное количество реплик на одной ноде._

_В режиме global каждый контейнер запустится на каждой ноде, но только по одному экземпляру._

---

- Какой алгоритм выбора лидера используется в Docker Swarm-кластере?

---

_— Для отказоустойчивой работы должно быть не менее 3 manager-нод.  
— Количество нод обязательно должно быть нечётным, но лучше не более 7 (рекомендация из документации Docker).  
— Среди manager-нод выбирается лидер, его задача гарантировать согласованность.  
— Лидер отправляет keepalive-пакеты с заданной периодичностью в пределах 150-300мс. Если пакеты не пришли, менеджеры начинают выборы нового лидера.  
— Если кластер разбит, нечётное количество нод должно гарантировать, что кластер останется консистентным, т.к. факт изменения состояния считается совершённым, если его отразило большинство нод, нечётное число гарантирует, что в какой-то части кластера будет большинство нод._

_Протокол решает проблему согласованности: чтобы все manager-ноды имели одинаковое представление о состоянии кластера._

---

- Что такое Overlay Network?

---

_Это сети, создаваемые поверх другой сети. Узлы оверлейной сети могут быть связаны либо физическим соединением, либо логическим, для которого в основной сети существуют один или несколько соответствующих маршрутов из физических соединений._

---

## Задача 2

Создайте ваш первый Docker Swarm-кластер в Яндекс Облаке.

Чтобы получить зачёт, предоставьте скриншот из терминала (консоли) с выводом команды:

```bash
docker node ls
```

---

```bash
cd packer
packer validate centos-7-base.json
packer build centos-7-base.json

cd ../terraform
yc iam service-account create service
```

```text
id: ajepmsc2r21eel0m4clo
folder_id: b1ghqou3mjgcc42lmpit
created_at: "2023-07-28T14:45:41.923113221Z"
name: service
```

```bash
yc resource-manager folder add-access-binding netology --role editor --subject serviceAccount:ajepmsc2r21eel0m4clo

yc iam key create --service-account-name service --output key.json
```

_Изменить variables.tf ._

_Включить VPN для операции init._

```bash
terraform init
terraform apply
```

```bash
ssh centos@158.160.50.70
sudo docker node ls
```

```text
[centos@node01 ~]$ sudo docker node ls
ID                            HOSTNAME             STATUS    AVAILABILITY   MANAGER STATUS   ENGINE VERSION
l9qvyigd712jgv0ego3ei56fy *   node01.netology.yc   Ready     Active         Leader           24.0.5
kw8er08kvrj025do6rkgomrfq     node02.netology.yc   Ready     Active         Reachable        24.0.5
ktfnsk5f735et3klnc8h4uwfr     node03.netology.yc   Ready     Active         Reachable        24.0.5
z77s5m8i5tq8xu5fjee1fi1nq     node04.netology.yc   Ready     Active                          24.0.5
ohtnusf09mzlklmr2aq7yu30i     node05.netology.yc   Ready     Active                          24.0.5
8ppg1v2arzkq1rwlzo1frijoo     node06.netology.yc   Ready     Active                          24.0.5
```

---

## Задача 3

Создайте ваш первый, готовый к боевой эксплуатации кластер мониторинга, состоящий из стека микросервисов.

Чтобы получить зачёт, предоставьте скриншот из терминала (консоли), с выводом команды:

```bash
docker service ls
```

---

![05-virt-05-docker-swarm](05-virt-05-docker-swarm.png)

---

## Задача 4 (*)

Выполните на лидере Docker Swarm-кластера команду, указанную ниже, и дайте письменное описание её функционала — что она делает и зачем нужна:

```bash
# см.документацию: https://docs.docker.com/engine/swarm/swarm_manager_locking/
docker swarm update --autolock=true
```

---

_autolock блокирует ноду после перезапуска до ввода ключа шифрования. Это нужно, чтобы защитить лог Raft и "секреты", сохранённые на ноде, которые теперь будут зашифрованы._

---
